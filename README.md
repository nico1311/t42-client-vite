# t42-client-vite
Vite + React project with Material UI, unit testing with Jest + Testing Library, and integration testing with Cypress.

## Scripts
* `npm run dev`: Start Vite dev server
* `npm run build`: Create a production build
* `npm run serve`: Preview production build
* `npm run prettier`: Find code-style issues with Prettier (it only checks but doesn't modify files)
* `npm run prettier:fix`: Run prettier and fix the code style issues
* `npm run test`: Run unit tests
* `npm run cypress`: Open Cypress for integration testing
* `npm run docs`: Generate docs with JSDoc
